var copy = {
  external: {
    files: [
      { expand: true, cwd: 'external/angular-js/', src: ['angular.min.js'], dest: 'src/lib/angularjs' },
      { expand: true, cwd: 'external/angular-route/', src: ['angular-route.min.js'], dest: 'src/lib/angularjs' },
    ]
  },
  release: {
    files: [
      { expand: true, cwd: 'src', src: ['**', '!css/**', '!lib/**', '!leaflet/**', '!sass/**', '!scripts/**', '!ts/**', '!patches/**'], dest: 'dist/release' },
      { expand: true, cwd: 'src/lib/jquery-ui', src: ['images/*'], dest: 'dist/release/css' }, // niestety jquery-ui.css wciaga obrazki relatywnie do siebie
      { expand: true, cwd: 'src/leaflet', src: ['images/*'], dest: 'dist/release/css' },
      { expand: true, cwd: 'src/lib/promise-js', src: ['promise.js'], dest: 'dist/release/lib/promise-js' },     
      // { expand: true, cwd: 'src/lib', src: ['**/*', '!**/*.js', '!**/*.css', '!jquery-ui/images/*'], dest: 'dist/release', filter: 'isFile',
      //   rename: function(dest, src) {
      //     console.log(src, '=>', dest);
      //   var path = require('path');
      //   var pathSep = "/"; //path.sep - nie wiem dlaczego na windowsie pliki sa rozdzielone "/", zapisuje na sztywno
      //   // Remove the first 1 folders from src and place into dest
      //   var wobase = src.split(pathSep).slice(1).join(path.sep);
      //   var joined = path.join(dest, wobase);
      //     return joined;
      // }},
    ]
  },
  deploy: {
    files: [
       { expand: true, cwd: 'dist/release', src: ['**'], dest: '../smartsab_mobile/www' }
    ]
  }
};

module.exports = function (grunt) {
  grunt.config.set('copy', copy);
}
